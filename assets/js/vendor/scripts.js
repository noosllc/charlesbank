// CHARLESBANK WEBSITE
// October 2015
// Scripts and Plugins






// if page is scrolled, add collapsed nav class
$(function() {
	$(window).scroll(function() {    
		var scroll = $(window).scrollTop();
		if (scroll > 20) {
			$( ".top-bar" ).removeClass( "scroll-top" ).addClass( "scroll-down" );
			$( ".contain-to-grid" ).removeClass( "opacity80" ).addClass( "opacity95" );
		} else {
			$( ".top-bar" ).removeClass( "scroll-down" ).addClass( "scroll-top" );
			$( ".contain-to-grid" ).removeClass( "opacity95" ).addClass( "opacity80" );
		}
	});

});






// if page not at top and refreshed, reinstate the collapsed nav class
$(function() {
	var eTop = $('#main-content').offset().top;
	scrollCheck = (eTop - $(window).scrollTop());
	if (scrollCheck != 0) {
		$( ".top-bar" ).removeClass( "scroll-top" ).addClass( "scroll-down" ).css( "transition", "all 0" );
		$( ".contain-to-grid" ).removeClass( "opacity80" ).addClass( "opacity95" ).css( "transition", "all 0" );
	} else {
		$( ".top-bar" ).removeClass( "scroll-down" ).addClass( "scroll-top" ).css( "transition", "all 0" );
		$( ".contain-to-grid" ).removeClass( "opacity95" ).addClass( "opacity80" ).css( "transition", "all 0" );
	}
});






// phImage v1, allows image as placeholder in search field 
// copyright (c) 2015 Brett Marks (ai3 [at] internode.net.au)
// downloaded at: https://github.com/cyphix333/phImage

$(function () {
	
	$.fn.phImage = function(options) {
		
		// Set our default options whilst taking in any passed by the user
		var settings = $.extend({
			// These are the defaults
			remove_current: true,
			padding: 'default',
			background_position: null,
			background_repeat: 'no-repeat',
		}, options);	
		
		return this.each(function() {
			
			var element = $(this);
			
			// Get our background image
			var bg_image = element.attr('data-ph-image');
			
			if (bg_image) {
				
				if (settings.remove_current) {
					
					// Insert current placeholder text
					element.attr('placeholder', 'Search');
					
					element.on('focus', function(){
						// Remove background image
						element.css("background-image", "none");
						element.css("padding-left", "1em");
						element.attr('placeholder', '');
					});
					
					element.on('blur', function(){
						// Put background image back?
						if (element.val() == "") {
							element.css("background-image", "url('" + bg_image + "')");
							element.css("padding-left", "3em");
							element.attr('placeholder', 'Search');

						}
					});
					
				} else {
					
					if (settings.padding != 'default') {
						// Let's set the padding of the element to what was provided
						element.css("padding", settings.padding);
					}
					
				}
				
				/*
					We apply the background image initially below, but only if they have chosen to leave the placeholder text in
					as the placeholder text in (the background image always should show even if the field has a value) OR if they
					have chosen to not show the placeholder text AND the field is empty
				 */
				
				if (!settings.remove_current || element.val() == "") {
					// Now let's set it as the field background
					element.css({"background-image": "url('" + bg_image + "')", "background-repeat": settings.background_repeat});	
			
				} else {
					element.css("background-repeat", settings.background_repeat);
				}
				
				// Apply background position if provided
				
				if (settings.background_position) {
					element.css("background-position", settings.background_position);
				}
				
			}
			
		});
		
	};
	
});






// foundation select dropdown menu styling
// downloaded at: https://github.com/roymckenzie/foundation-select

(function ( $ ) {

  $.fn.foundationSelect = function() {

    // Check to see if custom dropdowns have already been drawn
    if (!$('.custom-dropdown-area').length) {

      // If custom dropdowns haven't been drawn, build and insert them
      return this.each(function () {
        selectPrompt = '';
        selected = '';
        translateClasses = '';
        select = $(this);
        selectId = select.attr('id');
        multiple = false;
        multiple = select.prop('multiple') ? true : false;
        options = '';
        if (select.data('prompt')) {
          selectPrompt = '<span class="default-label">' + select.data('prompt') + '</span>';
          options = '<li class="disabled">' + selectPrompt + '</li>';
        } else {
          selectPrompt = 'Choose...';
        }
        select.find('option').each( function () {
          if ($(this).attr('selected')) {
            selected = 'selected';
            selectPrompt = "<div class='" + $(this).attr('class') + "'>" + $(this).html() + "</div>";
          }
          if( $(this).attr('class') ) {
            translateClasses = $(this).attr('class') + ' ';
          }
          options += '<li data-value="' + this.value + '" class="' + translateClasses + selected + '"><span class="option-title">' + $(this).html() + '</span></li>';
          selected = '';
        });
        newButton = '<div class="custom-dropdown-area" data-orig-select="#' + selectId + '"' + (multiple ? ' data-multiple="true"' : '') + '><a href="#" data-dropdown="select-' + selectId + '" class="custom-dropdown-button">' + selectPrompt + '</a> \
        <ul id="select-' + selectId + '" class="f-dropdown custom-dropdown-options" data-dropdown-content> \
          ' + options + ' \
        </ul></div>';
        select.hide();
        select.after(newButton);
      });
    };
  };

  // setup a listener to deal with custom dropdown clicks.
  $(document).on('click', '.custom-dropdown-area li', function () {
    if ($(this).hasClass('disabled')) {
      return false;
    }
    dropdown = $(this).closest('.custom-dropdown-area');
    multiple = dropdown.data('multiple') ? true : false;
    text = "<div class='" + $(this).attr('class') + "'>" + $(this).find('.option-title').html() + "</div>";
    value = $(this).data('value');
    totalOptions = dropdown.find('li').not('.disabled').length;
    origDropdown = $(dropdown.data('orig-select'));
    prompt = origDropdown.data('prompt') ? origDropdown.data('prompt') : 'Choose...';
    if (multiple) {
      $(this).toggleClass('selected');
      selectedOptions = [];
      selectedTitles = [];
      dropdown.find('.selected').each( function () {
        selectedOptions.push($(this).data('value'));
        selectedTitles.push($(this).find('.option-title').html());
      });
      origDropdown.val(selectedOptions);
      if (selectedOptions.length) {
        if (selectedOptions.length > 2) {
          dropdown.find('.custom-dropdown-button').html(selectedOptions.length + ' of ' + totalOptions + ' selected');
        }else{
          dropdown.find('.custom-dropdown-button').html(selectedTitles.join(', '));
        }
      }else{
        dropdown.find('.custom-dropdown-button').html(prompt);
      }
    }else{
      dropdown.find('li').removeClass('selected');
      Foundation.libs.dropdown.close($('#'+dropdown.find('ul').attr('id')));
      origDropdown.val(value);
      $(this).toggleClass('selected');
      dropdown.find('.custom-dropdown-button').html(text);
    }
  });

  $(document).on('reset', 'form', function () {
    if ($(this).children('.custom-dropdown-area').length) {
      $(this).find('.custom-dropdown-area').each( function () {
        origDropdown = $($(this).data('orig-select'));
        dropdown = $(this);
        multiple = dropdown.data('multiple') ? true : false;
        dropdown.find('li').removeClass('selected');
        if (origDropdown.data('prompt')) {
          prompt = origDropdown.data('prompt');
        }else{
          origDropdown.find('option').each( function () {
            if ($(this).attr('selected')) {
              prompt = $(this).html();
              dropdown.find('li[data-value="' + this.value + '"]').addClass('selected');
            }
          });
          if (prompt == '') {
            prompt = 'Choose...';
          }
        }
        dropdown.find('.custom-dropdown-button').html(prompt);
      });
    }
  });

}( jQuery ));






// Sticky Plugin v1.0.3 for jQuery
// Author: Anthony Garand
// Date: 07/20/2015
// Website: http://stickyjs.com/
// Makes an element on the page stick on the screen as you scroll

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node/CommonJS
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {
    var slice = Array.prototype.slice; // save ref to original slice()
    var splice = Array.prototype.splice; // save ref to original slice()

  var defaults = {
      topSpacing: 0,
      bottomSpacing: 0,
      className: 'is-sticky',
      wrapperClassName: 'sticky-wrapper',
      center: false,
      getWidthFrom: '',
      widthFromWrapper: true, // works only when .getWidthFrom is empty
      responsiveWidth: false
    },
    $window = $(window),
    $document = $(document),
    sticked = [],
    windowHeight = $window.height(),
    scroller = function() {
      var scrollTop = $window.scrollTop(),
        documentHeight = $document.height(),
        dwh = documentHeight - windowHeight,
        extra = (scrollTop > dwh) ? dwh - scrollTop : 0;

      for (var i = 0, l = sticked.length; i < l; i++) {
        var s = sticked[i],
          elementTop = s.stickyWrapper.offset().top,
          etse = elementTop - s.topSpacing - extra;

	//update height in case of dynamic content
	s.stickyWrapper.css('height', s.stickyElement.outerHeight());

        if (scrollTop <= etse) {
          if (s.currentTop !== null) {
            s.stickyElement
              .css({
                'width': '',
                'position': '',
                'top': ''
              });
            s.stickyElement.parent().removeClass(s.className);
            s.stickyElement.trigger('sticky-end', [s]);
            s.currentTop = null;
          }
        }
        else {
          var newTop = documentHeight - s.stickyElement.outerHeight()
            - s.topSpacing - s.bottomSpacing - scrollTop - extra;
          if (newTop < 0) {
            newTop = newTop + s.topSpacing;
          } else {
            newTop = s.topSpacing;
          }
          if (s.currentTop !== newTop) {
            var newWidth;
            if (s.getWidthFrom) {
                newWidth = $(s.getWidthFrom).width() || null;
            } else if (s.widthFromWrapper) {
                newWidth = s.stickyWrapper.width();
            }
            if (newWidth == null) {
                newWidth = s.stickyElement.width();
            }
            s.stickyElement
              .css('width', newWidth)
              .css('position', 'fixed')
              .css('top', newTop);

            s.stickyElement.parent().addClass(s.className);

            if (s.currentTop === null) {
              s.stickyElement.trigger('sticky-start', [s]);
            } else {
              // sticky is started but it have to be repositioned
              s.stickyElement.trigger('sticky-update', [s]);
            }

            if (s.currentTop === s.topSpacing && s.currentTop > newTop || s.currentTop === null && newTop < s.topSpacing) {
              // just reached bottom || just started to stick but bottom is already reached
              s.stickyElement.trigger('sticky-bottom-reached', [s]);
            } else if(s.currentTop !== null && newTop === s.topSpacing && s.currentTop < newTop) {
              // sticky is started && sticked at topSpacing && overflowing from top just finished
              s.stickyElement.trigger('sticky-bottom-unreached', [s]);
            }

            s.currentTop = newTop;
          }
        }
      }
    },
    resizer = function() {
      windowHeight = $window.height();

      for (var i = 0, l = sticked.length; i < l; i++) {
        var s = sticked[i];
        var newWidth = null;
        if (s.getWidthFrom) {
            if (s.responsiveWidth) {
                newWidth = $(s.getWidthFrom).width();
            }
        } else if(s.widthFromWrapper) {
            newWidth = s.stickyWrapper.width();
        }
        if (newWidth != null) {
            s.stickyElement.css('width', newWidth);
        }
      }
    },
    methods = {
      init: function(options) {
        var o = $.extend({}, defaults, options);
        return this.each(function() {
          var stickyElement = $(this);

          var stickyId = stickyElement.attr('id');
          var stickyHeight = stickyElement.outerHeight();
          var wrapperId = stickyId ? stickyId + '-' + defaults.wrapperClassName : defaults.wrapperClassName;
          var wrapper = $('<div></div>')
            .attr('id', wrapperId)
            .addClass(o.wrapperClassName);

          stickyElement.wrapAll(wrapper);

          var stickyWrapper = stickyElement.parent();

          if (o.center) {
            stickyWrapper.css({width:stickyElement.outerWidth(),marginLeft:"auto",marginRight:"auto"});
          }

          if (stickyElement.css("float") === "right") {
            stickyElement.css({"float":"none"}).parent().css({"float":"right"});
          }

          stickyWrapper.css('height', stickyHeight);

          o.stickyElement = stickyElement;
          o.stickyWrapper = stickyWrapper;
          o.currentTop    = null;

          sticked.push(o);
        });
      },
      update: scroller,
      unstick: function(options) {
        return this.each(function() {
          var that = this;
          var unstickyElement = $(that);

          var removeIdx = -1;
          var i = sticked.length;
          while (i-- > 0) {
            if (sticked[i].stickyElement.get(0) === that) {
                splice.call(sticked,i,1);
                removeIdx = i;
            }
          }
          if(removeIdx !== -1) {
            unstickyElement.unwrap();
            unstickyElement
              .css({
                'width': '',
                'position': '',
                'top': '',
                'float': ''
              })
            ;
          }
        });
      }
    };

  // should be more efficient than using $window.scroll(scroller) and $window.resize(resizer):
  if (window.addEventListener) {
    window.addEventListener('scroll', scroller, false);
    window.addEventListener('resize', resizer, false);
  } else if (window.attachEvent) {
    window.attachEvent('onscroll', scroller);
    window.attachEvent('onresize', resizer);
  }

  $.fn.sticky = function(method) {
    if (methods[method]) {
      return methods[method].apply(this, slice.call(arguments, 1));
    } else if (typeof method === 'object' || !method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error('Method ' + method + ' does not exist on jQuery.sticky');
    }
  };

  $.fn.unstick = function(method) {
    if (methods[method]) {
      return methods[method].apply(this, slice.call(arguments, 1));
    } else if (typeof method === 'object' || !method ) {
      return methods.unstick.apply( this, arguments );
    } else {
      $.error('Method ' + method + ' does not exist on jQuery.sticky');
    }
  };
  $(function() {
    setTimeout(scroller, 0);
  });
}));

