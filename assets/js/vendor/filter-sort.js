var FilterSort = {
	/**
		@param key
		returns all elements with data-<key>
	*/
	getAllOf: function(key){
		return $('*[data-' + key + ']');
	},
	/**
		Reverse alphabetical sort on li members
	*/
	reverseSort: function(a,b){
		a = $(a).data('name');
		b = $(b).data('name');
		if (a<b) return 1;
		return (a>b)? -1: 0;
	},
	/**
		Alphabetical sort on li members
	*/
	alphaSort: function(a,b){
		a = $(a).data('name');
		b = $(b).data('name');
		if (a<b) return -1;
		return (a>b)? 1: 0;
	},
	/**
		Sort a single team
	*/
	sortWithin: function(outerKey, sortBy, order){
		var ul = $('ul[data-container="' + outerKey + '"]');
		var li = ul.children('li');
		li.detach().sort(order);
		ul.append(li);
	},
	
	hideAllExcept: function(element, keyName, keyValue, removeClear){
		var items = FilterSort.getAllOf(keyName);
		items.each(function(index, item){
			// if keyValue is falsey, show all
			if ( ($(item).data(keyName) === keyValue ) || !keyValue 	) {
				$(item).removeClass('unselect-hide');
				if (removeClear){
					$(item).css('clear','none');
				}
			} else {
				$(item).addClass('unselect-hide');
			}
		});
	},
	hideAllEmpty: function(element, keyName){
		var outerKey = 'name';
		var groups = FilterSort.getAllOf('category');
		var countVisible = 0;
		groups.each(function(index, item){
		
			$(item).removeClass('fs-hide');
			var visible = $(item).find('li:visible');
			if (visible.length === 0){
				$(item).addClass('fs-hide');
				countVisible++;
			}
			//console.log(item, visible.length);
			//console.log($(item).find('section').length);
			//console.log(countVisible, "HIDDEN");
		});
		if (countVisible === groups.length){
			$('#no-results').show();
		} else {
			$('#no-results').hide();		
		}
	},
	order: {}
}

FilterSort.order = {
	'A to Z': FilterSort.alphaSort,
	'Z to A': FilterSort.reverseSort
};

