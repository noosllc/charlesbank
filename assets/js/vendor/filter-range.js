var FilterRange = {

	parseAndNormalizeRange: function(rangeStr){
		var range = (rangeStr + "").toUpperCase().split(/-/);
		if (range[1]) {
			range[1] = range[1] + "ZZZ";
		}
		return range;
	},
	filter: function(rangeVal, attribute){
		var range = this.parseAndNormalizeRange(rangeVal);
		var hideClass = 'hide-' + attribute;
		if (range.length === 1) {
			this.getAllListItems().removeClass(hideClass);
		} else {
			var self = this;
			var dataAttr = 'data-' + attribute;
			this.getAllListItems().each( function(index, elmt){
				var filterValue = FilterRange.normalizeValue($(elmt).attr(dataAttr));
				var inRange = self.inRange(filterValue, range);
				if (inRange) {
					$(elmt).removeClass(hideClass);
				} else {
					$(elmt).addClass(hideClass);
				}
			})
		}
	},
	getAllListItems: function(){
		return $('p[data-name]');
	},
	normalizeValue: function(value){
		value = value.toUpperCase();
		return (value.indexOf('THE ') === 0)? value.substring(4): value;
	},
	inRange: function(value, range){
		if (range.length === 1) return range[0];
		return (range[0] <= value) && (value <= range[1]);
	}			
};